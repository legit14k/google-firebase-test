//
//  AddAssignmentViewController.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import Firebase

class AddAssignmentViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var assignmentNameTextField: UITextField!
    @IBOutlet weak var scoreTextField: UITextField!
    @IBOutlet weak var dateOutlet: UIDatePicker!
    @IBOutlet weak var addButtonOutlet: UIButton!
    @IBOutlet weak var navBarLabel: UINavigationItem!
    
    // Reference the database
    let ref = Database.database().reference(withPath: "students")
    // New title
    var addViewTitle = "Add Assignment"
    
    // MARK: - Loading
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add radius to button
        addButtonOutlet.layer.cornerRadius = 23
        
        // Change title of the nav bar
        navBarLabel.title = addViewTitle
        
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "backToHome", sender: self)
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        if assignmentNameTextField.text != "" && scoreTextField.text != "" {
            guard let text = assignmentNameTextField.text else { return }
            let assignmentItem = AssignmentList(firstName: "Jason", lastName: "Grimberg", addedByUser: "g.jason409@gmail.com", isTeacher: true, assignmentName: text, key: "students")
            //let assignmentItem = AssignmentList(firstName: "Jason", lastName: "Grimberg", addedByUser: "g.jason409@gmail.com", isTeacher: true)
            
            let assignmentItemRef = self.ref.child(text.lowercased())
            
            assignmentItemRef.setValue(assignmentItem.toAnyObject())
            // Clear the text fields
            assignmentNameTextField.text = ""
            scoreTextField.text = ""
            // Go back to the last controller
            self.performSegue(withIdentifier: "backToHome", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
