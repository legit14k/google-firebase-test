//
//  Assignments.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation

class Assignments {
    /* Stored Properites */
    let assignmentName: String
    
    /* Computed Properties */
    
    /* Initializers */
    init(assignmentName: String) {
        self.assignmentName = assignmentName
    }
    
    /* Methods */
}
