//
//  HomeTableViewCell.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var assignmentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
