//
//  HomeViewController.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/1/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import Firebase

// ReuseIdentifier
private let g_cellID1 = "Cell_ID1"

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    @IBOutlet weak var navBarLabel: UINavigationItem!
    @IBOutlet weak var addButtonOutlet: UIBarButtonItem!
    
    // MARK: Properties
    var user: Users!
    
    var items: [AssignmentList] = []
    // Tableview property
    var tableView = UITableView()
    // New title
    var homeTitle = "Assignments"
    // Reference the database
    let ref = Database.database().reference(withPath: "students")
    
    // Test string to send out to the database
    let testText = "89"
    
    // MARK: - Loading View
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the title
        navBarLabel.title = homeTitle
        
        // Test to view data coming in and out
        ref.observe(.value, with: { snapshot in
            print(snapshot.value as Any)
        })
        
//        // Set a new assignment item
//        let assignmentItem = Assignments(assignmentName: testText)
//        // Set the child to the reference
//        let assignmentItemRef = ref.child("assignment1".lowercased()) // Set to each student
//
//        assignmentItemRef.setValue(assignmentItem.assignmentName)
        
        ref.queryOrdered(byChild: "students").observe(.value, with: { snapshot in
            var newItems: [AssignmentList] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let groceryItem = AssignmentList(snapshot: snapshot) {
                    newItems.append(groceryItem)
                }
            }
            
            print(newItems)
            
            self.items = newItems
            self.tableView.reloadData()
        })
        
        // Delegates and Data source
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Reload the table view data
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Reuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: g_cellID1, for: indexPath) as! HomeTableViewCell
        
        // Configure Cell
        cell.assignmentLabel.text = items[indexPath.row].getFullName()
        
        // Return our configured cell
        return cell
    }
    
    // MARK: - Actions
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "toAddView", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
