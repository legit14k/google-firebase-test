//
//  LoginViewController.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 11/30/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButtonOutlet: UIButton!
    
    // Reference the database
    let ref = Database.database().reference(withPath: "JasonGrimebrg") // Set path to specific user
    
    // MARK: - UI Stuff
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add radius to the button
        logInButtonOutlet.layer.cornerRadius = 23
        
        // Add observer when the keyboard shows and hides
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // Move the view up when the keyboard is shown
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= 275
        }
    }
    // Move the view down when the keyboard is hidden
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += 275
        }
    }
    
    // MARK: - Actions
    
    // MARK: - Controlling the Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Move to the next text field when the retrun key is pressed
        if textField == userNameTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            textField.resignFirstResponder()
        }
        return true
    }

    @IBAction func logInTapped(_ sender: UIButton) {
        // Authorize the email and password
        Auth.auth().signIn(withEmail: userNameTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                // Segue to the next controller after authentication
                self.performSegue(withIdentifier: "fromLogIn", sender: self)
            }
            else {
                // Show error if the wrong username or password is given
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? HomeViewController {
            
            // NOTE: - Send over data needed for user data
            
            // Send over a test string
            let test = [AssignmentList]()
            
            destination.items = test
        }
    }
}
