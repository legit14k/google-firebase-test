//
//  AssignmentList.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/8/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation
import Firebase

struct AssignmentList {
    
    // All variables
    let ref: DatabaseReference?
    let key: String
    let firstName: String
    let lastName: String
    let addedByUser: String
    var isTeacher: Bool
    let assignmentName: String
    
    // Initialization
    init(firstName: String, lastName: String, addedByUser: String, isTeacher: Bool, assignmentName: String, key: String = "") {
        self.ref = nil
        self.key = key
        self.assignmentName = assignmentName
        self.firstName = firstName
        self.lastName = lastName
        self.addedByUser = addedByUser
        self.isTeacher = isTeacher
    }
    
    // Get the snapshot of the data
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let assignmentName = value["assignment"] as? String,
            let firstName = value["firstName"] as? String,
            let lastName = value["lastName"] as? String,
            let addedByUser = value["addedByUser"] as? String,
            let isTeacher = value["completed"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.assignmentName = assignmentName
        self.firstName = firstName
        self.lastName = lastName
        self.addedByUser = addedByUser
        self.isTeacher = isTeacher
    }
    
    // Get the Full name of the current user
    func getFullName() -> String {
        let fullName = firstName + " " + lastName
        
        return fullName
    }
    
    // Set the whole object with the correct data
    func toAnyObject() -> Any {
        return [
            "assignment": assignmentName,
            "firstName": firstName,
            "lastName": lastName,
            "addedByUser": addedByUser,
            "completed": isTeacher
        ]
    }
}
