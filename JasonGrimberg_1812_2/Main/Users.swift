//
//  Users.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 12/8/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation
import Firebase

struct Users {
    
    let uid: String
    let email: String
    
    init(authData: Firebase.User) {
        uid = authData.uid
        email = authData.email!
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
}
