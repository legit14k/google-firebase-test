//
//  ViewController.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 11/23/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var logInOutlet: UIButton!
    @IBOutlet weak var signInOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set corner radius for the buttons
        logInOutlet.layer.cornerRadius = 23
        signInOutlet.layer.cornerRadius = 23
        
        // Change the title to the app title
        self.title = "The Books"
        
    }

    // MARK: - Actions
    @IBAction func logInTapped(_ sender: UIButton) {
        
//        // Segue if the user is already logged in
//        if Auth.auth().currentUser != nil {
//            self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
//        } else {
//            self.performSegue(withIdentifier: "logInView", sender: nil)
//        }
        
        self.performSegue(withIdentifier: "logInView", sender: nil)
    }
    
    @IBAction func signUpTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "signUpView", sender: nil)
    }
}
