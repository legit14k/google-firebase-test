//
//  SignUpViewController.swift
//  JasonGrimberg_1812_2
//
//  Created by Jason Grimberg on 11/30/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    @IBOutlet weak var isTeacher: UISwitch!
    
    var isTeacherBool = false
    
    // Reference the database
    let ref = Database.database().reference(withPath: "students")
    
    // MARK: - UI Stuff
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add radius to the sign in button
        signUpButtonOutlet.layer.cornerRadius = 23
        
        // Always set to student
        isTeacher.isOn = true
        
    }
    
    // MARK: - Actions
    
    @IBAction func signUpTapped(_ sender: UIButton) {
        // Make sure the passwords match
        // Show the user what went wrong
        if passwordTextField.text != repeatPasswordTextField.text {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please re-type password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            // Authorize the users email and password
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){ (user, error) in
                if error == nil {
                    self.performSegue(withIdentifier: "fromSignUp", sender: self)
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - Controlling the Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Move to the next text field when the return key is pressed
        if textField == firstNameTextField {
            textField.resignFirstResponder()
            lastNameTextField.becomeFirstResponder()
        } else if textField == lastNameTextField {
            textField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            textField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            textField.resignFirstResponder()
            repeatPasswordTextField.becomeFirstResponder()
        } else if textField == repeatPasswordTextField {
            textField.resignFirstResponder()
        }
        return true
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? HomeViewController {
            // NOTE: - Send over data needed for user data
            
            // Check the switch to see if this is a student
            if isTeacher.isOn == true {
                isTeacherBool = false
            } else {
                isTeacherBool = true
            }
            
            // NOTE: - Append to the database the newly created user
            
            let assignmentItem = AssignmentList(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, addedByUser: emailTextField.text!, isTeacher: isTeacherBool, assignmentName: "", key: "students")
            
            let fullName = assignmentItem.getFullName()
            
            let assignmentItemRef = self.ref.child(fullName.lowercased())
            
            assignmentItemRef.setValue(assignmentItem.toAnyObject())
            
            // Send over the newly created user
            var newUser = [AssignmentList]()
            
            newUser.append(AssignmentList(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, addedByUser: emailTextField.text!, isTeacher: isTeacherBool, assignmentName: ""))
            
            destination.items = newUser
        }
    }
}
